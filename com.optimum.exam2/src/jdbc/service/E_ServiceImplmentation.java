package jdbc.service;

import java.util.InputMismatchException;
import java.util.Scanner;

import jdbc.dao.E_DAO;
import jdbc.dao.E_DAOImplementation;
import jdbc.pojo.User;



public class E_ServiceImplmentation implements E_Service{
	E_DAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	//this is user choice method
	@Override
	public void userChoice() {
		var showMenu=true;
		try {
		while(showMenu==true) {
			
			System.out.println("2.Logout");
			System.out.println("Enter Choice");

			scannerRef = new Scanner(System.in);
			int choice = scannerRef.nextInt();
			switch (choice) {
			case 1:
				
				break;
			case 2:
				userCloseConnection();
				showMenu=false;
				break;
			case 3:
				
				break;
			case 4:
				break;
			case 5:
				
				break;
			default:
				System.out.println("Option not found..");
				break;

			}
		}
		}catch (InputMismatchException e) {
			System.out.println("Please enter a number");
			userChoice();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void userAuthentication() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter User name:");
		String userId = scannerRef.next();
		System.out.println("Enter password:");
		String userPassword = scannerRef.next();
		
		
		refUser = new User();
		try {
		refUser.setUser_ID(userId);
		refUser.setUser_password(userPassword);
		
		
		refUserDAO = new E_DAOImplementation();
		
		if (refUserDAO.userLogin(refUser)) {
			System.out.println("login successfull");
			userChoice();
		} 
		else {
			System.out.println();
			//throw new Exception();
			
		}
		
		} catch ( ArithmeticException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Wrong username/password");
			userAuthentication();
		}
		
	}

	@Override
	public void userCloseConnection() {
		refUserDAO = new E_DAOImplementation();
		refUserDAO.closeConnection(refUser);
		
		
	}

}
