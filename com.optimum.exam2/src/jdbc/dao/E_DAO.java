package jdbc.dao;

import jdbc.pojo.User;

public interface E_DAO {
	void closeConnection(User refUser);
	boolean userLogin(User refUser);
}
