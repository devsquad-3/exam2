package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.pojo.User;
import utility.DBUtility;

public class E_DAOImplementation implements E_DAO{
	
	Connection refConnection=null;
	PreparedStatement refPreparedStatement =null;
	@Override
	public void closeConnection(User refUser) {
		System.out.println("Logging out");
		refUser=null;
		try {
			refConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Closing Connection..");
			System.exit(0);
		}
		
		
	}

	@Override
	public boolean userLogin(User refUser) {
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "select user_password,user_id from user where user_id=?";
			
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			System.out.println();
			
			refPreparedStatement.setString(1, refUser.getUser_ID());
			ResultSet rs=refPreparedStatement.executeQuery();
			rs.next();
			
			if(rs.getString("user_id").equals(refUser.getUser_ID())
					&&(rs.getString("user_password").equals(refUser.getUser_password()))) {
				
				return true;
			}
			
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
		}
		
		finally {
			
		}
		return false;
	}

}
